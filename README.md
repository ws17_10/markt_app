# Wochen- und Trödelmarkt App

Diese App soll es Usern in Berlin ermöglichen Informationen über sich in der Nähe befindene Trödel und Wochenmärkte zu finden. Wochenmärkte in der Nähe können über eine Karte abgerufen werden. Außerdem lassen sich Infos zu Kontaktdaten und Öffnungszeiten anzeigen und nach bestimmten Märkten über eine integrierte Suchfunktion suchen.

Zielgruppe dieser App sind eigentlich alle, die viel unterwegs sind und gerne Marktbummeln. Da es in Berlin unzählige Märkte gibt, erleichtert es den Usern, Märkte aufzusuchen, die sich gerade in der Nähe oder auf dem Weg befinden.

## App benutzen

Einfach die App starten und über die Karte einen Marker finden. Klickt dann auf den Marken, um euch weitere Details anzuzeigen.
Über das Navigationsmenü oben links könnt ihr auch auf die Suchfunktion zugreifen.
Weitere Details findet ihr im [Konzept](app_konzept_final.pdf).

### Installation

Repository herunterladen, entpacken und mit Android Studio compilieren. Dann entweder apk builden und auf dem Smartphone starten oder direkt über Android Studio im Emulator.
Alternativ kann auch einfach die vorgebaute [apk](app/release/app-release.apk) benutzt werden.

## Author

* **Pascal Disse** - Immatrikulationsnummer: 547168

## Open Data
* [Wochen- und Trödelmärkte](http://www.berlin.de/sen/web/service/maerkte-feste/wochen-troedelmaerkte/)

## Libraries
* [Osmdroid](https://github.com/osmdroid/osmdroid)
* [Anko-Commons](https://github.com/Kotlin/anko/wiki/Anko-Commons-%E2%80%93-Misc)

## Repository
* [bitbucket.org/ws17_10/markt_app](https://bitbucket.org/ws17_10/markt_app)

## Version
Aktuelle Version ist:
1.0
