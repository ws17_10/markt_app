package com.example.pascal.marktapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.example.pascal.marktapp.datastructs.MarketEntity
import com.example.pascal.marktapp.utils.MARKET_ID_KEY
import com.example.pascal.marktapp.utils.get
import com.example.pascal.marktapp.utils.marketEntites
import com.example.pascal.marktapp.utils.setText
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.app_bar.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class DetailsActivity : AppCompatActivity(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)

        val id = intent.getStringExtra(MARKET_ID_KEY)
        info("Opened intent with id($id)")
        val marketEntity = marketEntites?.get(id)
        if (marketEntity != null) {
            info("MarketEntity found with id(${marketEntity.id})")
            setupTextViews(marketEntity)
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                // setup the action bar home button to act like the back button
                // this way the state of MainActivity is preserved (map doesn't reload, navigation doesn't change)
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupTextViews(marketEntity: MarketEntity) {
        district_view.setText(
                prefix = getString(R.string.market_district),
                content = marketEntity.district)

        location_view.setText(
                prefix = getString(R.string.market_location),
                content = marketEntity.location)

        days_view.setText(
                prefix = getString(R.string.market_days),
                content = marketEntity.days)

        times_view.setText(
                prefix = getString(R.string.market_times),
                content = marketEntity.times)

        manager_view.setText(
                prefix = getString(R.string.market_manager),
                content = marketEntity.manager)

        email_view.setText(
                prefix = getString(R.string.market_email),
                content = marketEntity.email)

        website_view.setText(
                prefix = getString(R.string.market_website),
                content = marketEntity.website)

        comments_view.setText(
                prefix = getString(R.string.market_comments),
                content = marketEntity.comments)
    }

}
