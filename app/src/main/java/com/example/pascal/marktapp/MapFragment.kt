package com.example.pascal.marktapp

import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager.LoaderCallbacks
import android.support.v4.content.AsyncTaskLoader
import android.support.v4.content.Loader
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pascal.marktapp.datastructs.MarketEntity
import com.example.pascal.marktapp.utils.*
import kotlinx.android.synthetic.main.fragment_map.*
import org.jetbrains.anko.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus
import org.osmdroid.views.overlay.OverlayItem
import java.net.URL


class MapFragment : Fragment(), AnkoLogger, LoaderCallbacks<String>, LocationListener {

    // minimum distance change before updating location
    private val LOC_UPDATE_MIN_DIST = 10f
    // minimum time between location updates in milliseconds
    private val LOC_UPDATE_MIN_TIME = 10000L

    //open map at these coordinates
    private val startPoint = GeoPoint(52.5145, 13.3501)

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        setupOsmDroidPreferecnces(activity.applicationContext)

        return inflater!!.inflate(R.layout.fragment_map, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupMap(map_view)

        location_button.setOnClickListener { onLocationButtonPressed() }

        loaderManager.initLoader(MAP_LOADER_ID, null, this)
    }

    private fun setupOsmDroidPreferecnces(applicationContext: Context) {
        val configurationInstance = Configuration.getInstance()
        configurationInstance.load(
                applicationContext,
                PreferenceManager.getDefaultSharedPreferences(applicationContext))

        //setup user agent to prevent getting banned from osm servers
        configurationInstance.userAgentValue = BuildConfig.APPLICATION_ID
    }

    private fun setupMap(map: MapView) {

        map.setTileSource(TileSourceFactory.MAPNIK)

        //add zoom buttons and zoom with 2 fingers:
        map.setBuiltInZoomControls(true)
        map.setMultiTouchControls(true)

        //setup camera position and zoom
        val mapController = map.controller
        mapController.setZoom(18)
        mapController.setCenter(startPoint)
    }

    private fun setupMapMarkers(map: MapView, marketEntites: ArrayList<MarketEntity>) {
        val markers = ArrayList<OverlayItem>()
        marketEntites.mapTo(markers) {
            OverlayItem(
                    it.id,
                    it.location,
                    "",
                    GeoPoint(it.lat, it.lon)
            )
        }
        val mapOverlay = ItemizedOverlayWithFocus<OverlayItem>(activity, markers, MarketMarkerOverlay(activity))
        mapOverlay.setFocusItemsOnTap(true)
        map.overlays.add(mapOverlay)
    }

    private fun onLocationButtonPressed() {
        if (!hasPermissionAccessFineLocation()) {
            requestAccessFineLocation(MAP_ACCESS_FINE_LOCATION)
        } else {
            focusCameraOnCurrentLocation()
        }
    }

    private fun focusCameraOnCurrentLocation() {
        val locationManager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOC_UPDATE_MIN_TIME, LOC_UPDATE_MIN_DIST, this)
            val location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (location != null) {
                map_view.controller.setCenter(GeoPoint(location))
            } else {
                //if location cannot be found show info toast
                activity.longToast(getString(R.string.map_location_access_error_message))
            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MAP_ACCESS_FINE_LOCATION ->
                if (permissionGranted(grantResults)) {
                    focusCameraOnCurrentLocation()
                } else {
                    activity.longToast(getString(R.string.map_location_permission_error_message))
                }
        }
    }


    override fun onCreateLoader(id: Int, args: Bundle?): Loader<String> {
        val url = buildUrl("")
        info("Url built: $url")
        return MapLoader(activity, url)
    }

    override fun onLoaderReset(loader: Loader<String>?) {}

    override fun onLoadFinished(loader: Loader<String>?, data: String?) {
        if (data == null) {
            error("Data in OnLoadFinished is null")
        } else {
            info("Response in OnLoadFinished: $data")
            val _marketEntites = createMarketEntitiesFromJSONString(data)
            if (_marketEntites != null) {
                marketEntites = _marketEntites
                setupMapMarkers(map_view, _marketEntites)
            }
        }
    }

    private class MapLoader(context: Context, val url: URL?) : AsyncTaskLoader<String>(context) {

        private var data: String? = null //member variable to cache data
        override fun loadInBackground(): String? {
            if (url != null) {
                data = url.readText()
            }
            return data
        }

        override fun onStartLoading() {
            if (data != null) {
                deliverResult(data)
            } else {
                forceLoad()
            }
        }

    }

    class MarketMarkerOverlay(private val activity: Context) : ItemizedIconOverlay.OnItemGestureListener<OverlayItem>, AnkoLogger {

        override fun onItemSingleTapUp(index: Int, item: OverlayItem?): Boolean {
            if (item != null) {
                info("Marker clicked with id(${item.uid})")
                activity.startActivity<DetailsActivity>(MARKET_ID_KEY to item.uid)
            } else {
                error("Marker OverlayItem is null")
            }
            return true
        }

        override fun onItemLongPress(index: Int, item: OverlayItem?): Boolean {
            return false
        }
    }

    //required empty methods for locationListener
    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {}

    override fun onProviderEnabled(p0: String?) {}
    override fun onProviderDisabled(p0: String?) {}
    override fun onLocationChanged(p0: Location?) {}

}
