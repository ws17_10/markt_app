package com.example.pascal.marktapp


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager.LoaderCallbacks
import android.support.v4.content.AsyncTaskLoader
import android.support.v4.content.Loader
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.pascal.marktapp.datastructs.MarketEntity
import com.example.pascal.marktapp.utils.*
import kotlinx.android.synthetic.main.fragment_search.*
import org.jetbrains.anko.*
import java.net.URL


class SearchFragment : Fragment(), AnkoLogger, LoaderCallbacks<String> {

    private val SEARCH_STRING_MIN_LEN = 3
    private lateinit var searchResultsAdapter: ArrayAdapter<MarketEntity>


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        search_button.setOnClickListener { onSearchButtonClicked() }
        setupResultsList()
    }

    private fun setupResultsList() {
        searchResultsAdapter = ArrayAdapter(
                activity,
                android.R.layout.simple_list_item_1)

        results_list.adapter = searchResultsAdapter

        results_list.onItemClickListener =
                AdapterView.OnItemClickListener { adapter, _, i, _ ->
                    val marketEntity = adapter.getItemAtPosition(i) as MarketEntity
                    activity.startActivity<DetailsActivity>(
                            MARKET_ID_KEY to marketEntity.id
                    )
                }
    }

    private fun onSearchButtonClicked() {
        val searchString = search_text.text.toString()
        if (validSearch(searchString)) {
            startSearchLoader(searchString)
        } else {
            showInvalidInputPopup()
        }
    }

    private fun startSearchLoader(searchString: String) {
        val searchLoader = loaderManager.getLoader<String>(SEARCH_LOADER_ID)
        val queryBundle = Bundle()
        queryBundle.putString(SEARCH_QUERY_KEY, searchString)
        if (searchLoader == null) {
            loaderManager.initLoader(SEARCH_LOADER_ID, queryBundle, this)
        } else {
            loaderManager.restartLoader(SEARCH_LOADER_ID, queryBundle, this)
        }
    }

    private fun validSearch(searchString: String): Boolean {
        return (searchString.length >= SEARCH_STRING_MIN_LEN)
    }

    private fun showInvalidInputPopup() {
        activity.alert(
                getString(R.string.search_input_error_message, SEARCH_STRING_MIN_LEN),
                getString(R.string.search_input_error_title)
        ) { okButton { } }.show()
    }

    private fun showEmptyResponsePopup() {
        activity.alert(
                getString(R.string.search_result_error_message),
                getString(R.string.search_result_error_title)
        ) { okButton { } }.show()
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<String>? {
        return if (args != null) {
            val url = buildUrl(args.getString(SEARCH_QUERY_KEY))
            info("Url built: $url")
            SearchLoader(activity, url)
        } else {
            error("Loader has no arguments")
            null
        }
    }

    override fun onLoaderReset(loader: Loader<String>?) {
        //required empty method body
    }

    override fun onLoadFinished(loader: Loader<String>?, data: String?) {
        if (data != null) {
            info("Data in OnLoadFinished: $data")
            val searchResults = createMarketEntitiesFromJSONString(data)
            if (searchResults != null && !searchResults.isEmpty()) {
                searchResultsAdapter.clear()
                searchResultsAdapter.addAll(searchResults)
            } else {
                showEmptyResponsePopup()
            }
        } else {
            error("Data in OnLoadFinished is null")
        }
    }

    private class SearchLoader(context: Context, val url: URL?) : AsyncTaskLoader<String>(context) {
        private var data: String? = null //member variable to cache data
        override fun loadInBackground(): String? {
            if (url != null) {
                data = url.readText()
            }
            return data
        }

        override fun onStartLoading() {
            if (data != null) {
                deliverResult(data)
            } else {
                forceLoad()
            }
        }

    }
}
