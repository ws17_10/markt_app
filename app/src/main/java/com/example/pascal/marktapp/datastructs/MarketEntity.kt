package com.example.pascal.marktapp.datastructs

data class MarketEntity(
        val id: String,
        var district: String,
        var location: String,
        var lat: Double,
        var lon: Double,
        var days: String,
        var times: String,
        var manager: String,
        var email: String,
        var website: String,
        var comments: String
){
    override fun toString(): String {
        return "$district\n$location"
    }
}