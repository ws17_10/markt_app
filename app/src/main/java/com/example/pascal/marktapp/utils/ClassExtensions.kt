package com.example.pascal.marktapp.utils

import android.Manifest
import android.content.pm.PackageManager
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.TextView
import com.example.pascal.marktapp.datastructs.MarketEntity

/**
* extends String class to handle "," as a decimal point when parsing to double
*/
fun String.fromGermanToDouble() : Double?{
    val correctDecimal = replace(",",".")
    try {
        return correctDecimal.toDouble()
    }catch (e: NumberFormatException){
        e.printStackTrace()
    }
    return null
}

/**
 * extends ArrayList<MarketEntity> so that entry can be found by id
 */
fun ArrayList<MarketEntity>.get(id : String):MarketEntity?{
    for (marketEntity in this){
        if (marketEntity.id == id){
            return marketEntity
        }
    }
    Log.e(this.toString(), "Could not with MarketEntry with id($id)")
    return null
}

/**
 * extends TextView so that text can have a prefix and be hidden if content is empty
 */
fun TextView.setText(prefix: String, content: String){
    if (content.isBlank()){
        visibility = View.GONE
    }else{
        text = "$prefix: $content"
    }
}


/**#########################################################################################
 * Permission extensions
 * #########################################################################################
 */

/**
 * extends Fragment to easily access Location Permission Status
 */
fun Fragment.hasPermissionAccessFineLocation(): Boolean{
    val permissionCheck = ContextCompat.checkSelfPermission(activity,
            Manifest.permission.ACCESS_FINE_LOCATION)
    return (permissionCheck == PackageManager.PERMISSION_GRANTED)
}

/**
 * extends Fragment to easily request Location Permission
 */
fun Fragment.requestAccessFineLocation(requestCode: Int){
    requestPermissions(
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            requestCode)
}

/**
 * helper method to make checking permission reponse more readable
 */
fun permissionGranted(grantResults: IntArray): Boolean{
    // If request is cancelled, the result arrays are empty.
    return grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
}