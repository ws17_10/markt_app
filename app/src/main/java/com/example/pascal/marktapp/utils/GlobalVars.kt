package com.example.pascal.marktapp.utils

import com.example.pascal.marktapp.datastructs.MarketEntity

const val MAP_LOADER_ID = 10
const val SEARCH_LOADER_ID = 11
const val SEARCH_QUERY_KEY = "loader_search_query"
const val MARKET_ID_KEY = "mid"
const val MAP_ACCESS_FINE_LOCATION = 20

var marketEntites: ArrayList<MarketEntity>? = null