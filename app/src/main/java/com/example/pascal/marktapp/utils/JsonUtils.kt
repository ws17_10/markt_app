package com.example.pascal.marktapp.utils

import com.example.pascal.marktapp.datastructs.MarketEntity
import org.json.JSONException
import org.json.JSONObject

private const val KEY_CONTENT = "index"
private const val KEY_ID = "id"
private const val KEY_DISTRICT = "bezirk"
private const val KEY_LOCATION = "location"
private const val KEY_LAT = "latitude"
private const val KEY_LON = "longitude"
private const val KEY_DAYS = "tage"
private const val KEY_TIMES = "zeiten"
private const val KEY_MANAGER = "betreiber"
private const val KEY_EMAIL = "email"
private const val KEY_WEBSITE = "www"
private const val KEY_COMMENTS = "bemerkungen"

@Throws(JSONException::class)
private fun creatMarketEntityFromJSONObject(jsonObject: JSONObject): MarketEntity? {

    val id = jsonObject.getString(KEY_ID)
    val district = jsonObject.getString(KEY_DISTRICT)
    val location = jsonObject.getString(KEY_LOCATION)
    val lat = jsonObject.getString(KEY_LAT).fromGermanToDouble()
    val lon = jsonObject.getString(KEY_LON).fromGermanToDouble()
    val days = jsonObject.getString(KEY_DAYS)
    val times = jsonObject.getString(KEY_TIMES)
    val manager = jsonObject.getString(KEY_MANAGER)
    val email = jsonObject.getString(KEY_EMAIL)
    val website = jsonObject.getString(KEY_WEBSITE)
    val comments = jsonObject.getString(KEY_COMMENTS)

    return if (lat != null && lon != null) {
        MarketEntity(id, district, location, lat, lon, days, times, manager, email, website, comments)
    }else{
        //since this is a map based app, markets without lat/lon are of no use to us
        null
    }
}

/**
 * turns json string data and turns it into MarketEntites
 *
 * @param data json string response of the "wochen-troedelmaerkte" api
 * @return an ArrayList of MarketEntities
 */
fun createMarketEntitiesFromJSONString(data: String): ArrayList<MarketEntity>? {
    try {
        val content = JSONObject(data).getJSONArray(KEY_CONTENT)
        val marketEntities = ArrayList<MarketEntity>()

        for (i in 0 until content.length()) {
            val jsonObject = content.getJSONObject(i)
            val marketEnitity = creatMarketEntityFromJSONObject(jsonObject)
            if (marketEnitity != null) {
                marketEntities.add(marketEnitity)
            }
        }
        return marketEntities

    } catch (e: JSONException) {
        e.printStackTrace()
        return null
    }
}

