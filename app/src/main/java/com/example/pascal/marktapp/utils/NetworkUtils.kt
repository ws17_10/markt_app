package com.example.pascal.marktapp.utils

import android.net.Uri
import java.net.MalformedURLException
import java.net.URL


private const val API_BASE_URL = "http://www.berlin.de/sen/web/service/maerkte-feste/wochen-troedelmaerkte/index.php/index/all.json"
private const val QUERY_KEY = "q"


/**
 * Builds the URL used to query the api
 *
 * @return The URL to use.
 */
fun buildUrl(queryValue: String): URL? {
    val uriBuilder = Uri.parse(API_BASE_URL).buildUpon()
    uriBuilder.appendQueryParameter(QUERY_KEY, queryValue)
    val builtUri = uriBuilder.build()
    return try {
        URL(builtUri.toString())
    } catch (e: MalformedURLException) {
        e.printStackTrace()
        null
    }
}
